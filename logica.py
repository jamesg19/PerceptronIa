from random import random

import numpy as np

from perceptron import Perceptron


class Logica:
    def __init__(self):
        # Iteraciones de entrenamiento
        #self.epochs = 1000


        pass

    def iniciar(self):


        #[X1,X2,RESULTADO]
        # Datos de entrada (compuerta OR)
        #inputs = np.array([[0, 1, 1], [1, 0, 1], [0, 0, 0], [1, 1, 1]])

        # Datos de entrada (compuerta AND)# Datos de entrada (compuerta AND)
        inputs = np.array([[0, 0, 0], [0, 1, 0], [1, 0, 0], [1, 1, 1]])

        # Datos de entrada (compuerta XOR)# Datos de entrada (compuerta XOR)
        #inputs = np.array([[0, 0, 0], [0, 1, 1], [1, 0, 1], [1, 1, 0]])


        #Datos iniciales
        lambaa=0.2
        w1= 0.90
        w2= 0.99
        u=0.4
        perceptronList=[]
        self.contador=0
        contradorError=0

        #for epoch in range(self.epochs):
        while True:
            self.contador +=1
            for subarray in inputs:
                #self.epochs +=1
                #x1, w1, x2, w2, resultado, lamb, u
                x1=subarray[0]
                x2=subarray[1]
                resultado=subarray[2]

                if self.contador  ==1:
                    tmpPerceptron = Perceptron(x1, w1, x2, w2, resultado, lambaa, u)
                    perceptronList.append(tmpPerceptron)
                    contradorError+= tmpPerceptron.e
                    print(tmpPerceptron.imprimir())
                else:
                    print( self.contador)
                    tmpAnt= perceptronList[-1]
                    tmpPerceptron = Perceptron(x1, tmpAnt.newW1, x2, tmpAnt.newW2, resultado, lambaa, tmpAnt.newU)
                    perceptronList.append(tmpPerceptron)
                    contradorError += tmpPerceptron.e
                    print(tmpPerceptron.imprimir())
            if contradorError == 0: break
            else: contradorError=0

        print("Iteraciones: ",self.contador )


        # Crear el perceptrón
        perceptron = perceptronList[-1]
        # Probar el perceptrón con los nuevos datos
        self.probar(perceptron, 1, 0)
        self.probar(perceptron, 0, 1)
        self.probar(perceptron, 0, 0)
        self.probar(perceptron, 1, 1)

    def probar(self, perceptron, X1, X2):
        # Calcular la salida del perceptrón con los nuevos valores
        umbral = (X1 * perceptron.newW1) + (X2 * perceptron.newW2)
        z = umbral - perceptron.newU
        activacion = perceptron.step_function(z)

        # Imprimir resultados
        print("Para X1 =", X1, " y X2 =", X2," = ", activacion)
