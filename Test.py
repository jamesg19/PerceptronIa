import math
import random


# Función sigmoide
def sigmoid(x):
    return 1 / (1 + math.exp(-x))

# Función tangente hiperbólica
def tanh(x):
    return (math.exp(x) - math.exp(-x)) / (math.exp(x) + math.exp(-x))

# Función identidad
def identity(x):
    return x

# Función step
def step(x):
    return 1 if x >= 0 else 0

# Inicialización de pesos y sesgos
def initialize_weights_biases(num_inputs, num_hidden_layers, num_neurons_per_layer, num_outputs):
    weights = []
    biases = []

    # Capa de entrada a primera capa oculta
    weights.append([[random.uniform(-1, 1) for _ in range(num_inputs)] for _ in range(num_neurons_per_layer[0])])
    biases.append([random.uniform(-1, 1) for _ in range(num_neurons_per_layer[0])])

    # Capas ocultas intermedias
    for i in range(1, num_hidden_layers):
        weights.append([[random.uniform(-1, 1) for _ in range(num_neurons_per_layer[i - 1])] for _ in range(num_neurons_per_layer[i])])
        biases.append([random.uniform(-1, 1) for _ in range(num_neurons_per_layer[i])])

    # Última capa oculta a capa de salida
    weights.append([[random.uniform(-1, 1) for _ in range(num_neurons_per_layer[-1])] for _ in range(num_outputs)])
    biases.append([random.uniform(-1, 1) for _ in range(num_outputs)])

    return weights, biases

# Propagación hacia adelante
def forward_propagation(inputs, weights, biases, hidden_activation, output_activation):
    activations = [inputs]
    for i in range(len(weights)):
        weighted_sum = [sum(activations[-1][j] * weights[i][k][j] for j in range(len(weights[i][k]))) + biases[i][k] for k in range(len(weights[i]))]
        if i == len(weights) - 1:
            activations.append([output_activation(x) for x in weighted_sum])
        else:
            activations.append([hidden_activation(x) for x in weighted_sum])
    return activations

# Retropropagación
def backpropagation(inputs, targets, weights, biases, hidden_activation, output_activation, learning_rate):
    activations = forward_propagation(inputs, weights, biases, hidden_activation, output_activation)
    deltas = [[] for _ in range(len(weights))]
    deltas[-1] = [activations[-1][i] - targets[i] for i in range(len(targets))]

    for i in range(len(weights) - 1, -1, -1):
        if i == len(weights) - 1:
            deltas[i] = [deltas[i][j] * output_activation(activations[i + 1][j]) * (1 - output_activation(activations[i + 1][j])) for j in range(len(deltas[i]))]
        else:
            weighted_deltas = [sum(deltas[i + 1][j] * weights[i + 1][j][k] for j in range(len(weights[i + 1]))) for k in range(len(weights[i]))]
            deltas[i] = [weighted_deltas[j] * hidden_activation(activations[i + 1][j]) * (1 - hidden_activation(activations[i + 1][j])) for j in range(len(weighted_deltas))]

    for i in range(len(weights)):
        for j in range(len(weights[i])):
            for k in range(len(weights[i][j])):
                weights[i][j][k] -= learning_rate * deltas[i][j] * activations[i][k]
            biases[i][j] -= learning_rate * deltas[i][j]

# Entrenamiento de la red neuronal
def train_neural_network(num_inputs, num_outputs, num_hidden_layers, num_neurons_per_layer, hidden_activation_func, output_activation_func, learning_rate, num_epochs, training_data):
    weights, biases = initialize_weights_biases(num_inputs, num_hidden_layers, num_neurons_per_layer, num_outputs)

    if hidden_activation_func.lower() == "sigmoid":
        hidden_activation = sigmoid
    elif hidden_activation_func.lower() == "tanh":
        hidden_activation = tanh
    else:
        print("Función de activación para capas ocultas no válida. Se utilizará la función sigmoide por defecto.")
        hidden_activation = sigmoid

    if output_activation_func.lower() == "identity":
        output_activation = identity
    elif output_activation_func.lower() == "step":
        output_activation = step
    else:
        print("Función de activación para capa de salida no válida. Se utilizará la función step por defecto.")
        output_activation = step

    for epoch in range(num_epochs):
        for inputs, targets in training_data:
            backpropagation(inputs, targets, weights, biases, hidden_activation, output_activation, learning_rate)
        mse = sum(sum((output_activation(forward_propagation(inputs, weights, biases, hidden_activation, output_activation)[-1][i]) - targets[i]) ** 2 for i in range(len(targets))) for inputs, targets in training_data) / (len(training_data) * len(targets))
        print(f"Epoch {epoch + 1}: MSE = {mse}")

    return weights, biases

def probar(inputs, weights, biases, hidden_activation, output_activation):
    activations = forward_propagation(inputs, weights, biases, hidden_activation, output_activation)
    return activations[-1]

# Ejemplo de uso
num_inputs = int(input("Ingrese el número de entradas: "))
num_outputs = int(input("Ingrese el número de salidas: "))
num_hidden_layers = int(input("Ingrese el número de capas ocultas: "))
num_neurons_per_layer = []
for i in range(num_hidden_layers):
    num_neurons_per_layer.append(int(input(f"Ingrese el número de neuronas para la capa oculta {i + 1}: ")))

hidden_activation_func = input("Ingrese la función de activación para las capas ocultas (sigmoid o tanh): ")
output_activation_func = input("Ingrese la función de activación para la capa de salida (identity o step): ")

learning_rate = 0.1
num_epochs = 1000
training_data = [
    ([0, 0], [0]),
    ([0, 1], [1]),
    ([1, 0], [1]),
    ([1, 1], [0])
]

weights, biases = train_neural_network(num_inputs, num_outputs, num_hidden_layers, num_neurons_per_layer, hidden_activation_func,
                                       output_activation_func, learning_rate, num_epochs, training_data)
# Datos de prueba
test_inputs = [
    [0, 0],
    [0, 1],
    [1, 0],
    [1, 1]
]

# Probar la red neuronal con los nuevos datos
for inputs in test_inputs:
    output = probar(inputs, weights, biases, sigmoid,step)
    print(f"Entradas: {inputs}, Salida: {output}")