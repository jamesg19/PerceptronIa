import numpy as np



class Perceptron:
    def __init__(self,x1,w1,x2,w2,resultado,lamb,u):
        self.lambdaa=lamb
        self.x1= x1
        self.w1 = w1
        self.x2 = x2
        self.w2 = w2
        self.u= u
        self.s = -1*(self.u)
        #calculos
        self.y= resultado
        self.umbral= self.umbral_function(self.x1,self.w1,self.x2,self.w2,self.s)
        self.z= self.z_umbral_function(self.x1,self.w1,self.x2,self.w2,self.s)
        self.activacion = self.step_function(self.z)
        self.e= self.e_function(self.y,self.activacion)

        self.diffU = self.diffU(self.e)
        self.newU=self.newU(self.u,self.diffU)

        self.diffW1=self.diffW(self.e,self.x1)
        self.newW1=self.newW(self.w1,self.diffW1)

        self.diffW2 = self.diffW(self.e, self.x2)
        self.newW2 = self.newW(self.w2, self.diffW2)

        self.e_percent=self.e_percentage(self.activacion,self.y)

    # Activacion
    def step_function(self, z):
        return 1 if z >= 0 else 0

    '''# activacion = u*x1 + w2*x2 +(-0.2)=0             SI Z>0 1 ; 0
    def y_function(self,x1,x2):
        z=(self.u*x1)+(self.w2*x2)-0.2
        y=self.step_function(z)
        return y,z'''

    def z_umbral_function(self,x1,w1,x2,w2,s):
        #umbral=(x1*w1)+(x2*w2)
        z=round(self.umbral+(s),4)
        return z
    def umbral_function(self,x1,w1,x2,w2,s):
        umbral=(x1*w1)+(x2*w2)
        return umbral

    def e_function(self,y,activacion):
        e=(y-activacion)
        if e>=0:
            return e
        else:
            return e

    def diffU(self,e):
        return (self.lambdaa*e)*(-1)

    def newU(self,u,diffU):
        return (u+diffU)

    def diffW(self,e,x):
        return (self.lambdaa*e*x)

    def newW(self,w,diffW):
        return (w+diffW)

    def e_percentage(self, activacion, y):
        try:
            if y != 0:
                temp = abs(((activacion - y) / y) * 100)
                return temp
            else:
                return 0
        except :
            print("Error")
            # Manejo de la división por cero
            return 0

    def imprimir(self):
        print(" x1: ",  self.x1, " w1: ",  self.w1, " x2: ",  self.x2, " w2: ",  self.w2, " u: ",  self.u, " s: ", self.s, " y: ",  self.y," umbral: ", self.umbral, " z: ", self.z, " activacion: ", self.activacion, " e: ", self.e, " diffU: ", self.diffU, " newU: ", self.newU, " diffW1: ", self.diffW1, " newW1: ", self.newW1, " diffW2: ", self.diffW2, " newW2: ", self.newW2, " e_percent: ", self.e_percent)





#z=X1*W1-U

#z= x1*w1 + x2*w2 +s
#mayor umbral=  (x1*w1) +(x2*w2)