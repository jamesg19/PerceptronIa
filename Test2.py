import numpy as np

# Definición de la función sigmoide
def sigmoid(x):
    return 1 / (1 + np.exp(-x))

# Definición de la función step
def step(x):
    return np.where(x >= 0, 1, 0)

# Función para inicializar los pesos de la red neuronal de manera aleatoria
def initialize_weights(input_size, hidden_layers, hidden_neurons, output_size):
    layers = [input_size] + hidden_neurons + [output_size]
    weights = []
    for i in range(len(layers) - 1):
        weight_matrix = np.random.randn(layers[i], layers[i+1])
        weights.append(weight_matrix)
    return weights

# Función para la propagación hacia adelante (forward propagation)
def forward_propagation(inputs, weights):
    activations = [inputs]
    for i in range(len(weights)):
        activations.append(sigmoid(np.dot(activations[-1], weights[i])))
    return activations

# Función para la retropropagación del error (backpropagation)
def backward_propagation(inputs, outputs, weights, activations, learning_rate):
    error = outputs - activations[-1]
    adjustments = [error * sigmoid_derivative(activations[-1])]
    for i in range(len(weights) - 1, 0, -1):
        error = adjustments[-1].dot(weights[i].T)
        adjustments.append(error * sigmoid_derivative(activations[i]))
    adjustments.reverse()

    for i in range(len(weights)):
        weights[i] += activations[i].T.dot(adjustments[i]) * learning_rate

# Función para la derivada de la función sigmoide
def sigmoid_derivative(x):
    return x * (1 - x)


# Función para entrenar la red neuronal
def train_network(inputs, outputs, hidden_layers, hidden_neurons, output_activation, learning_rate, epochs):
    input_size = inputs.shape[1]
    output_size = outputs.shape[1]
    weights = initialize_weights(input_size, hidden_layers, hidden_neurons, output_size)

    for epoch in range(epochs):
        activations = forward_propagation(inputs, weights)
        backward_propagation(inputs, outputs, weights, activations, learning_rate)

        if epoch % 100 == 0:
            loss = np.mean(np.square(outputs - activations[-1]))
            print(f"Epoch {epoch}: Loss = {loss}")

    return weights

# Ejemplo de uso
if __name__ == "__main__":
    # Definir datos de entrada y salida de ejemplo
    inputs = np.array([[0, 0], [0, 1], [1, 0], [1, 1]])
    outputs = np.array([[0], [1], [1], [0]])



    # Definir arquitectura de la red neuronal
    input_size = inputs.shape[1]
    hidden_layers = 8
    hidden_neurons = [1000]  # Una capa oculta con 4 neuronas
    output_activation = step
    learning_rate = 0.3
    epochs = 15000

    # Entrenar la red neuronal
    trained_weights = train_network(inputs, outputs, hidden_layers, hidden_neurons, output_activation, learning_rate, epochs)

    # Datos de entrada para probar
    test_inputs = np.array([[0, 0], [0, 1], [1, 0], [1, 1]])

    # Realizar propagación hacia adelante con los pesos entrenados
    predictions = forward_propagation(test_inputs, trained_weights)[-1]

    # Redondear las predicciones para convertirlas en 0 o 1
    rounded_predictions = predictions

    # Mostrar las predicciones
    print("Predicciones:")
    for i in range(len(test_inputs)):
        print(f"Entrada: {test_inputs[i]}, Predicción: {rounded_predictions[i]}")